// @ts-nocheck
import * as React from 'react';
import { DatePicker } from '@material-ui/pickers';

export interface DeliveryDateProps {
  selectedDate: Date | null;
  setSelectedDate: (date: Date | null) => void;
}

const DeliveryDatePicker: React.FC<DeliveryDateProps> = ({
  selectedDate,
  setSelectedDate,
}): React.ReactElement => {
  return (
    <DatePicker
      fullWidth
      inputVariant="outlined"
      label="Delivery Date"
      value={selectedDate}
      disablePast
      onChange={setSelectedDate}
    />
  );
};

export default DeliveryDatePicker;
