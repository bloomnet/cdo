export { default as HomePage } from './home/HomePage';
export { default as FloristDetailsPage } from './florist-details/FloristDetailsPage';
export { default as OurFloristsPage } from './our-florists/OurFloristsPage';
export { default as PageNotFound } from './other/PageNotFound';

// Base Url for all images.
// http://directory.bloomnet.net/bloomnet-images/ads/{fileName}
export const baseImageUrl = 'http://directory.bloomnet.net/bloomnet-images/ads/';
